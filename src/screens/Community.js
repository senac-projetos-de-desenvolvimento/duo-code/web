import React, { useEffect } from "react";
import { Button } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import SearchBox from "../components/community/SearchBox";
import networkIcon from "../icons/community/network.png";
import javascriptIcon from "../icons/community/javascript.png";
import AskingPanel from "../components/community/askingPanel";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import searchIcon from "../icons/community/search.png";
import Input from "reactstrap/lib/Input";

export default function CommunityPage() {
  const [page, setPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [askings, setAskings] = React.useState([]);
  const [search, setSearch] = React.useState("");

  const setPageState = (event, value) => {
    setPage(value);
  };

  const setSearchField = (e) => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    getSpotlightAskings();
  }, []);

  useEffect(() => {
    searchSpotlightAskings();
  }, [search]);

  useEffect(() => {
    getSpotlightAskings(page);
  }, [page]);

  const getSpotlightAskings = async (page = null) => {
    API.get("community/spotlightAskings/" + page, { courseId: 1 })
      .then((response) => {
        if (response.status === 200) {
          setAskings(response.data.askings);
          setTotalPages(response.data.totalPages);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const searchSpotlightAskings = async () => {
    if (search.length == 0) {
      console.log("en")
      getSpotlightAskings(1);
    }

    API.get("community/spotlightAskings/search/" + search)
      .then((response) => {
        if (response.status === 200) {
          setAskings(response.data.askings);
          // setTotalPages(response.data.totalPages);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="row mt-4">
      <div className="col-12 mb-5 mt-2">
        <div className="z-index-2 d-flex justify-content-center align-items-center">
          <img
            src={networkIcon}
            style={{ width: "100px", marginRight: "2vw" }}
          />
          <h3>Comunidade</h3>
        </div>
      </div>

      <div className="col-12 d-flex justify-content-center">
        <div className="d-inline-block ">
          <div>
            <div className="d-inline-block">
              <img className="" src={searchIcon} alt="" style={{ width: "30px" }} />
            </div>
            <div className="d-inline-block mx-3">
              <Input className="" value={search} name="search" id="search" onChange={setSearchField} placeholder="Pesquisa" />
            </div>
          </div>
        </div>
        <div className="d-inline-block">
          <Button tag={Link} to="community/newQuestion" color="success">
            Nova Pergunta
          </Button>
        </div>
      </div>
      <div className="col-12 d-flex justify-content-center">
        <div className="col-10 mt-4">
          {askings.map((ask) => {
            return <AskingPanel ask={ask} />;
          })}
        </div>
      </div>
      <div className="col-12 d-flex justify-content-center mt-5">
        <Pagination
          count={totalPages}
          color="primary"
          name="page"
          page={page}
          onChange={setPageState}
          variant="outlined"
        />
      </div>
    </div>
  );
}
