import React, { Component } from "react";
import { Link } from "react-router-dom";
import API from "../api";
import CourseTree from "../components/CourseTree";
import medal from "../icons/course/medal.png";

class LearnTree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modules: [],
      level: null,
      totalXp: 0,
      finished: false,
      hash: null,
    };
  }

  componentDidMount() {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.getModules();
  }

  getModules() {
    API.post("/course/modules", { courseId: this.props.match.params.id })
      .then((response) => {
        this.setState({ modules: response.data });
        this.getLevel();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getLevel() {
    API.post("/course/progress", { courseId: this.props.match.params.id })
      .then((response) => {
        this.setState({
          level: response.data.level,
          totalXp: response.data.totalXp,
          finished: response.data.finished,
          hash: response.data.hash
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <h1 className="d-flex justify-content-center mt-5">Árvore de Ensino</h1>
        <p className="d-flex justify-content-center mb-4">
          <b>Total de xp: &nbsp;</b> {this.state.totalXp}
        </p>
        {this.state.finished ?
          <div className="d-flex align-items-center justify-content-center medal">
            <img src={medal} alt="medalha" />
            <Link to={{ pathname: `/certificate/${this.state.hash}` }}>Certificado</Link>
          </div> : ""}
        <CourseTree
          modules={this.state.modules}
          courseId={this.props.match.params.id}
          level={this.state.level}
        />
      </div>
    );
  }
}

export default LearnTree;
