import React, { Component, Fragment } from "react";
import { Spinner } from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import API from "../api";
import Question from "../components/session/Question";
import ResultPage from "../components/session/Result";
import "../styles/session.css";
import "../styles/question.css";
import ErrorTip from "../components/ErrorTip";

export default class Session extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: {},
      numQuestion: 1,
      actualQuestion: {},
      questions: [],
      dataLoaded: false,
      answers: [],
      wrongs: [],
      review: false,
      errorTip: null,
      exit: false,
      totalErrors: 0,
      result_totalXp: "-",
      result_newLevel: undefined,
      loading: false,
      result_screen: false,
    };
  }

  componentDidMount() {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.getData();
  }

  getData = async () => {
    await this.getCourse(this.props.match.params.courseId);
    await this.getQuestions(this.props.match.params.moduleId);
  };

  barMoviment = async () => {
    const bar = document.getElementById("progressBar");
    const questionsLength = this.state.questions.length;
    const wrongsLength = this.state.wrongs.length;
    const numQuestion = this.state.numQuestion;
    bar.style.width = `${
      (numQuestion * 100) / (questionsLength + wrongsLength)
    }%`;
  };

  nextQuestion = async () => {
    if (this.state.numQuestion < this.state.questions.length) {
      this.setState({ errorTip: null });
      this.setState({ numQuestion: this.state.numQuestion + 1 });
      this.setState({
        actualQuestion: this.state.questions[this.state.numQuestion],
      });
      this.barMoviment();
    } else {
      this.setState({ errorTip: null });
      if (
        this.state.wrongs.length > 0 &&
        this.state.numQuestion - this.state.questions.length + 1 <=
          this.state.wrongs.length
      ) {
        this.setState({ numQuestion: this.state.numQuestion + 1 });
        this.setState({
          actualQuestion: this.state.wrongs[
            this.state.numQuestion - this.state.questions.length
          ],
        });
        this.barMoviment();
      } else {
        this.endSession();
      }
    }
  };

  endSession = async () => {
    this.setState({ loading: true });
    API.post("/course/session/end", {
      courseId: this.props.match.params.courseId,
      moduleId: this.props.match.params.moduleId,
      totalErrors: this.state.totalErrors,
    })
      .then((response) => {
        let data = response.data;
        this.setState({
          result_totalXp: data.totalXpGained,
          result_newLevel: data.newLevel,
        });
        this.setState({ loading: false, result_screen: true });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  verifyQuestion = async (opt) => {
    const options = this.state.actualQuestion.answer.split("*");
    const right = options[0];
    console.log(options, "right:", right, "opt:", opt);
    if (opt == right) {
      const answerObj = {
        questionId: this.state.actualQuestion.id,
        answer: opt,
      };
      let answers = this.state.answers;
      answers.push(answerObj);
      this.setState({ answers });
      this.nextQuestion();
    } else {
      this.setState({ errorTip: this.state.actualQuestion.tip });
      this.setState({ totalErrors: this.state.totalErrors + 1 });
      if (this.state.review === true && this.state.wrongs.length === 1) return;
      const wrongs = this.state.wrongs;
      wrongs.push(this.state.actualQuestion);
    }
  };

  firstQuestion = async () => {
    this.setState({ actualQuestion: this.state.questions[0] });
  };

  getCourse = async (id) => {
    API.get(`course/get/${id}`)
      .then((response) => {
        console.log(response.data);
        this.setState({ course: response.data });
        this.setState({ dataLoaded: true });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  getQuestions = async (moduleId) => {
    API.post(`course/session`, { moduleId })
      .then((response) => {
        console.log(response.data);
        this.setState({ questions: response.data });
        this.firstQuestion();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  exitSession = async () => {
    console.log("entrou");
    this.setState({ exit: true });
  };

  render() {
    return (
      <div className="mt-4">
        {this.state.result_screen ? (
          <ResultPage
            result_totalXp={this.state.result_totalXp}
            result_newLevel={this.state.result_newLevel}
            exitSession={this.exitSession}
          />
        ) : (
          <div>
            {this.state.loading ? (
              <Spinner animation="border" />
            ) : (
              <div className="main-board-session ml-4">
                <div>
                  <Link to={{ pathname: `/learn/${this.state.course.id}` }}>
                    <img
                      src={this.state.course.logoImage}
                      alt="logo"
                      id="courseImage"
                    />
                  </Link>
                  <div id="test">
                    <div id="bar">
                      <div id="progressBar"></div>
                    </div>
                  </div>
                </div>
                <div id="questionPanel">
                  <div id="questionPanelInside">
                    <div className="d-flex justify-content-center">
                      <Question
                        actualQuestion={this.state.actualQuestion}
                        verifyQuestion={this.verifyQuestion}
                        nextQuestion={this.nextQuestion}
                      />
                    </div>
                    <div className="col-12 d-flex justify-content-center tip-section">
                      {this.state.errorTip ? (
                        <ErrorTip
                          errorTip={this.state.errorTip}
                          nextQuestion={this.nextQuestion}
                        />
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div id="sessionFooter">
              <h6>@2020 DuoCode. Todos os direitos reservados</h6>
            </div>
          </div>
        )}
        {this.state.exit ? (
          <Redirect to={`/learn/${this.props.match.params.courseId}`} />
        ) : (
          ""
        )}
      </div>
    );
  }
}
