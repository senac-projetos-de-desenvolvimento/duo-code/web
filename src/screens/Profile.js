import React, { Component, Fragment } from "react";
import API from "../api";
import "../styles/coursePage.css";
import profileImage from "../icons/person.png";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }

  componentDidMount = () => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.getStats();
  };

  getStats = () => {
    API.get("/user/stats").then((response) => {
      console.log(response.data);
      this.setState({ user: response.data });
    });
  };

  render() {
    return (
      <Fragment>
        <div>
          <div className="">
            <h2 className="text-center mt-5">Perfil</h2>
            <div className="row">
              <img
                src={profileImage}
                alt="Perfil"
                className="profileImage"
                style={{ width: "100px" }}
              />
              <div className="d-flex align-items-center justify-content-center">
                <h3 className="ml-5">
                  <b>{this.state.user.username}</b>
                </h3>
              </div>
              <div></div>
            </div>
            <div className="mt-4 ml-5">
              <h6 style={{ color: "gray" }}>
                <b>Faculdade:</b>{" "}
                {this.state.user.college ? this.state.user.college.name : " - "}
              </h6>
              <h6 style={{ color: "gray" }}>
                <b>Total de Experiência:</b> {this.state.user.totalXp}
              </h6>
              <h6 style={{ color: "gray" }}>
                <b>Entrou em:</b>{" "}
                {new Date(this.state.user.createdAt).toLocaleDateString()}
              </h6>
            </div>
          </div>
          <div>
            <h3 className="mt-5 mb-5">Cursos</h3>
            {this.state.user.courses
              ? this.state.user.courses.map((course) => {
                  return (
                    <div className="row">
                      <div className="col-3">
                        <img
                          src={course.course.logoImage}
                          alt="logo de curso"
                          style={{ width: "120px" }}
                        />
                      </div>
                      <div className="col-9 row">
                        <div className="col-12">
                          <h4 className="float-left">{course.course.name}</h4>
                        </div>
                        <div className="col-12">
                          <h6 className="float-left" style={{ color: "gray" }}>
                            <b>Iniciado em:</b>{" "}
                            {new Date(course.createdAt).toLocaleDateString()}
                          </h6>
                        </div>
                        <div className="col-12">
                          <h6 className="float-left" style={{ color: "gray" }}>
                            <b>Pontos de Experiência:</b> {course.totalXp}
                          </h6>
                        </div>
                      </div>
                    </div>
                  );
                })
              : ""}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Profile;
