import React, { Component, Fragment } from "react";
import { Table } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import rankingIcon from "../icons/ranking.png";
import arrowIcon from "../icons/arrow.png";
import studentIcon from "../icons/ranking/students_ranking.png";
import institutionIcon from "../icons/ranking/institution.png";
import { Link } from "react-router-dom";

class RankingCollege extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ranking: [],
      page: 1,
    };
  }

  componentDidMount = () => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.getRankingColleges();
  };

  nextPage = async () => {
    this.setState({ page: this.state.page + 1 }, () => {
      this.getRankingColleges();
    });
  };

  previousPage = async () => {
    if (this.state.page <= 1) return;
    this.setState({ page: this.state.page - 1 }, () => {
      this.getRankingColleges();
    });
  };

  getRankingColleges = () => {
    API.get(`/ranking/colleges/${this.state.page}`)
      .then((response) => {
        this.setState({
          ranking: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <Fragment>
        <div className="ranking-icons row mt-5 mb-5">
          <div className="col-6 d-flex justify-content-center">
            <div>
              <Link to="/ranking/students">
                <img
                  src={studentIcon}
                  alt="Ranking de estudantes"
                  style={{ width: "80px" }}
                />
              </Link>
            </div>
          </div>
          <div className="col-6 d-flex justify-content-center">
            <div>
              <Link to="/ranking">
                <img
                  src={institutionIcon}
                  alt="Ranking entre instituições"
                  style={{ width: "100px" }}
                />
              </Link>
            </div>
          </div>
        </div>
        <div>
          <h4 className="mb-5">Ranking entre Instituições</h4>
        </div>
        <div className="row align-items-center">
          <div className="col-1">
            {this.state.page > 1 ? (
              <img
                src={arrowIcon}
                alt="Seta Esquerda"
                style={{ cursor: "pointer" }}
                onClick={() => this.previousPage()}
              />
            ) : (
              ""
            )}
          </div>
          <div className="col-10">
            <Table>
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Cidade</th>
                  <th>Estado</th>
                  <th>XP</th>
                </tr>
              </thead>
              <tbody>
                {this.state.ranking.map((college) => {
                  return (
                    <tr>
                      <td>{college.name}</td>
                      <td>{college.city}</td>
                      <td>{college.state}</td>
                      <td>{college.totalXp}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
          <div className="col-1">
            <img
              src={arrowIcon}
              alt="Seta Direita"
              style={{ transform: "scaleX(-1)", cursor: "pointer" }}
              onClick={() => this.nextPage()}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default RankingCollege;
