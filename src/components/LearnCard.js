import React from 'react'
import { Link } from 'react-router-dom'

const LearnCard = props => (
    <div className="card ml-4 mr-4" style={{ width: "10rem", textAlign: 'center' }}>
        <Link to={{ pathname: `/learn/${props.id}` }} >
            <div className="card-img-top-board">
                <img className="card-img-top" src={'' + props.logoImage} alt='Curso' />
            </div>
        </Link>
        <div className="card-body">
            <h4 className="card-title">{props.name}</h4>
            <p className="card-text" style={{ float: 'bottom' }}>...<br /></p>
        </div>
    </div >
)

export default LearnCard;