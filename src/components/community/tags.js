import React from "react";
import "../../styles/community/questionPage.css";

const TagsComponent = (props) => {
  return (
    <div>
      {props.tags
        ? props.tags.split(";").map((tag) => {
            return <span className="tag-item">{tag}</span>;
          })
        : ""}
    </div>
  );
};

export default TagsComponent;
