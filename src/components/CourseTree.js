import React from 'react'
import { Row } from 'reactstrap'
import { BrowserRouter as Router, Link, BrowserRouter } from 'react-router-dom'
import '../styles/courseTree.css'

const CourseTree = (props) => {
    let level = 0
    return (
        props.modules.map(modules => {
            return (
                <Row className="moduleTree">
                    {modules.map(module => {
                        return (
                            <div className="moduleCard">
                                <div className="divImage">
                                    <BrowserRouter>
                                        {props.level >= module.level ?
                                            <Link to={`/learn/${props.courseId}/session/${module.id}`} onClick={() => setTimeout(() => window.location.reload(), 500)}>
                                                <div className="borderImage moduleGreenColor">
                                                    <img className="image"
                                                        src={module.moduleImage}
                                                        alt="image"
                                                    />
                                                </div>
                                            </Link>
                                            :
                                            <div className="borderImage moduleGrayColor">
                                                <img className="image"
                                                    src={module.moduleImage}
                                                    alt="image"
                                                />
                                            </div>
                                        }
                                    </BrowserRouter>
                                </div>
                                <div className="bodyCard">
                                    <b><h5 className="bodyTitle">{module.name}</h5></b>
                                </div>
                            </div>
                        )
                    })}
                </Row>
            )
        })
    )
}

export default CourseTree