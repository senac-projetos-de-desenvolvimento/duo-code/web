import React from "react";

const info = (props) => {
  return (
    <div className="alert-success text-center">
      <p>{props.message}</p>
    </div>
  );
};

export default info;
